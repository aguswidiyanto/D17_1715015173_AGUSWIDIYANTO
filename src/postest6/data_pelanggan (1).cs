using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Hotel
{
    #region Data_pelanggan
    public class Data_pelanggan
    {
        #region Member Variables
        protected int _id;
        protected string _nama;
        protected string _IsiBiling;
        protected string _HabisBiling;
        #endregion
        #region Constructors
        public Data_pelanggan() { }
        public Data_pelanggan(string nama, string IsiBiling, string HabisBiling)
        {
            this._nama=nama;
            this._IsiBiling=IsiBiling;
            this._HabisBiling=HabisBiling;
        }
        #endregion
        #region Public Properties
        public virtual int Id
        {
            get {return _id;}
            set {_id=value;}
        }
        public virtual string Nama
        {
            get {return _nama;}
            set {_nama=value;}
        }
        public virtual string IsiBiling
        {
            get {return _IsiBiling;}
            set {_IsiBiling=value;}
        }
        public virtual string HabisBiling
        {
            get {return _HabisBiling;}
            set {_HabisBiling=value;}
        }
        #endregion
    }
    #endregion
}